<?php

namespace yii2portal\users\common\models;

use Yii;

/**
 * This is the model class for table "core_groups".
 *
 * @property integer $id
 * @property string $name
 * @property integer $system
 */
class CoreGroups extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%group}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['system'], 'integer'],
            [['name'], 'string', 'max' => 85],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'system' => 'System',
        ];
    }
}
